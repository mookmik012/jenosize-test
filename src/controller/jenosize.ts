import { Double } from 'bson';
import express from 'express'

const path = express()

path.get('/game24', async (req, res) => {

    const { number1, number2, number3, number4 } = req.body
    let nums1: Array<number> = [number1, number2, number3, number4];
    //console.log(solve(nums1));

    //let a: number[] = [nums[0], nums[1], nums[2], nums[3]];
    let a: number[] = [1, 1, 2, 6];
    //return this.helper(a);
    console.log(helper(a));
    res.json(helper(a) ? 'OK' : 'F')

})

// function class Solution {
//     public judgePoint24(nums: number[]): boolean {
//         let a: number[] = [nums[0], nums[1], nums[2], nums[3]];
//         return this.helper(a);
//     }

function/*private*/ helper(a: number[]): boolean {
        if (a.length === 1) return Math.abs(a[0] - 24) < 1.0E-4;
        for (let i: number = 0; i < a.length; i++) {
            {
                for (let j: number = i + 1; j < a.length; j++) {
                    {
                        let d: number[] = (s => { let a = []; while (s-- > 0) a.push(0); return a; })(a.length - 1);
                        for (let k: number = 0, index: number = 0; k < a.length; k++) {
                            {
                                if (k !== i && k !== j) {
                                    d[index++] = a[k];
                                }
                            };
                        }
                        {
                            let array16368 = compute(a[i], a[j]);
                            for (let index16367 = 0; index16367 < array16368.length; index16367++) {
                                let num = array16368[index16367];
                                {
                                    d[d.length - 1] = num;
                                    if (helper(d)) return true;
                                }
                            }
                        }
                    };
                }
            };
        }
        return false;
    }

    function/*private*/ compute(x: number, y: number): number[] {
        return [x + y, x - y, y - x, x * y, x / y, y / x];
    }
// }

/*
function solve(nums: Array<number>): boolean {    

    console.log(Math.abs(nums[0] - 24))
    if (nums.length == 0) return false;
    if (nums.length == 1) return Math.abs(nums[0] - 24) < 0.0001;

    for (let i = 0; i < nums.length; i++) {
        for (let j = i + 1; j < nums.length; j++) {

            if (i != j) {
                let nums2: Array<number> = [];

                for (let k = 0; k < nums.length; k++) if (k != i && k != j) {
                    nums2.push(nums[k]);
                }

               // console.log(nums2)
                for (let k = 0; k < 4; k++) {
                    if (k < 2 && j > i) continue;
                    if (k == 0) nums2.push(nums[i] + nums[j]);
                    if (k == 1) nums2.push(nums[i] * nums[j]);
                    if (k == 2) nums2.push(nums[i] - nums[j]);
                    if (k == 3) {
                        if (nums[j] != 0) {
                            nums2.push(nums[i] / nums[j]);
                        } else {
                            continue;
                        }
                    }
                    
                    if (solve(nums2)) return true;
                    nums2.splice(nums2.length - 1);

                }
            }

        }
    }
    return false;
}
*/

export class User {
    constructor() {
        let a: number[] = [1, 1, 2, 6];
        this.helper = this.helper.bind(a);
    }

    /*private*/ helper(a : number[]) : boolean {
        if(a.length === 1) return Math.abs(a[0] - 24) < 1.0E-4;
        for(let i : number = 0; i < a.length; i++) {{
            for(let j : number = i + 1; j < a.length; j++) {{
                let d : number[] = (s => { let a=[]; while(s-->0) a.push(0); return a; })(a.length - 1);
                for(let k : number = 0, index : number = 0; k < a.length; k++) {{
                    if(k !== i && k !== j) {
                        d[index++] = a[k];
                    }
                };}
                {
                    let array16368 = this.compute(a[i], a[j]);
                    for(let index16367=0; index16367 < array16368.length; index16367++) {
                        let num = array16368[index16367];
                        {
                            d[d.length - 1] = num;
                            if(this.helper(d)) return true;
                        }
                    }
                }
            };}
        };}
        return false;
    }

    /*private*/ compute(x : number, y : number) : number[] {
        return [x + y, x - y, y - x, x * y, x / y, y / x];
    }
}

export default path