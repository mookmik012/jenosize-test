import express from 'express'
import cors from 'cors'
import { config } from 'dotenv'
import path from 'path'
import bodyParser from 'body-parser'
import fs from 'fs'

import jenosizeController from './controller/jenosize'


config()

const port = process.env.PORT || 5000
const app = express()

app.use(cors())
app.use(bodyParser.json({ limit: '5mb' }))
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/api/', jenosizeController)

// let user = new User();
// app.get('/api/game24', user.helper.bind(user));

app.listen(port, () => {
  console.log(`Server started on port ${port}`)
})


